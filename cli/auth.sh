#!/bin/bash

set -e

. ./support/functions.sh

config_auth() {
  log_message "Configuring AWS IAM Auth method ..."

  data=$(
    cat <<EOF
{
  "description": "Commonwell AWS IAM authentication method",
  "type": "aws",
  "config": {
    "default_lease_ttl" : "10m",
    "max_lease_ttl" : "60m"
  }
}
EOF
  )

  http_req_string POST "$data" "$VAULT_ADDR/sys/auth/cmig/aws/iam"
}

config_local_role() {
  log_message "Configuring DevOps $1 Role for $2 ..."

  type=$1
  iam_role_arn=$2

  data=$(
    cat <<EOF
{
  "auth_type": "iam",
  "bound_iam_principal_arn": "$iam_role_arn",
  "ttl": "10m",
  "max_ttl": "30m",
  "policies": [
    "$type"
  ]
}
EOF
  )

  http_req_string POST "$data" "$VAULT_ADDR/auth/cmig/aws/iam/role/$type"
}

config_remote_role() {
  log_message "Configuring Application $1 Role for $2 ..."

  app=$1
  assumed_role_arn=$2
  remote_account_id=$3

  data=$(
    cat <<EOF
{
  "sts_role": "$assumed_role_arn"
}
EOF
  )

  http_req_string POST "$data" "$VAULT_ADDR/auth/cmig/aws/iam/config/sts/$remote_account_id"

  data=$(
    cat <<EOF
{
  "auth_type": "iam",
  "bound_iam_principal_arn": "$assumed_role_arn",
  "ttl": "10m",
  "max_ttl": "20m",
  "policies": [
    "cmig/$app"
  ]
}
EOF
  )

  http_req_string POST "$data" "$VAULT_ADDR/auth/cmig/aws/iam/role/$app"
}

load_acl_policy() {
  file=$1
  name=$2

  payload=$(jq -n --arg b "$(cat $file)" '{"policy": $b}')
  http_req_string PUT "$payload" "$VAULT_ADDR/sys/policy/$name"
  http_req_string PUT "$payload" "$VAULT_ADDR/sys/policy/$name"
}

load_acl_policies() {
  load_acl_policy "../acl_policies/admin.hcl" "admin"
  load_acl_policy "../acl_policies/provisioner.hcl" "provisioner"
  load_acl_policy "../acl_policies/guidewire.hcl" "cmig/guidewire"
  load_acl_policy "../acl_policies/microservice.hcl" "cmig/microservice"
}

config_auth_roles() {
  config_local_role admin $HUB_IAM_ARN
  config_local_role provisioner $HUB_IAM_ARN
  config_local_role guidewire $HUB_IAM_ARN
  config_remote_role microservice $ENV_IAM_ARN $ENV_ACCOUNT_ID
}

execute_auth() {
  config_auth
  load_acl_policies
  config_auth_roles
}

. ./env_vars.sh
. ./functions.sh

set -e

init_acl() {
  http_notoken_req_empty PUT "$CONSUL_ADDR/acl/bootstrap"
}

init() {
  acl_bootstrap=$(init_acl | jq .)

  export CONSUL_TOKEN="$(echo $acl_bootstrap | jq -r '.SecretID')"
  export AUTH_TOKEN="X-Consul-Token: ${CONSUL_TOKEN}"

  log_message "Store Consul ACL Root token in $AWS_REGION secrets manager"
  aws secretsmanager create-secret --region $AWS_REGION --name "$CONSUL_CLUSTER_NAME-acl" --description "Consul ACL Root Token" --secret-string "$acl_bootstrap"
}

config_vault_acl() {
  #acl_rules=$(jq -n --arg b "$(cat acl.json)" '$b')

  payload=$(
    cat <<EOF
{
  "Name": "vault_storage",
  "Description": "Vault storage ACL policy",
  "Rules": "{\n  \"key_prefix\": {\n    \"vault\/\": {\n      \"policy\": \"write\"\n    }\n  },\n  \"node_prefix\": {\n    \"\": {\n      \"policy\": \"write\"\n    }\n  },\n  \"service\": {\n    \"vault\": {\n      \"policy\": \"write\"\n    }\n  },\n  \"agent_prefix\": {\n    \"\": {\n      \"policy\": \"write\"\n    }\n\n  },\n  \"session_prefix\": {\n    \"\": {\n      \"policy\": \"write\"\n    }\n  }\n}"
}
EOF
  )

  http_req_string PUT "$payload" "$CONSUL_ADDR/acl/policy"
}

init
config_vault_acl
